(function($) {

  "use strict";

  Drupal.behaviors.semantria_testing = {
    attach: function(context, settings) {
      $('.semantria-testing-form').once('semantria-testing-form--initialized').each(function() {

        // Checks only if an Id is provided.
        var form = $(this);
        var unique_id = form.find('input[name="unique_id"]').val();
        if (unique_id) {

          // Builds check url.
          var url = window.location.href.split('?')[0];
          url += '/response-check/' + unique_id;

          var check_for_semantria_response = function() {
            $.get(url, {"_": $.now()}, function(data) {

              // Checks indefinitely until data is fetched.
              if (!data) {
                setTimeout(check_for_semantria_response, 5000);
              }
              else {

                // Loads data.
                var configuration_id = $('<dt>' + Drupal.t('Configuration Id') + '</dt><dd>' + data[0].config_id + '</dd>');
                var status = $('<dt>' + Drupal.t('Document Status') + '</dt><dd>' + data[0].status + '</dd>');
                var sentiment = $('<dt>' + Drupal.t('Sentiment') + '</dt><dd>' + data[0].sentiment_polarity + '</dd>');
                var score = $('<dt>' + Drupal.t('Score') + '</dt><dd>' + data[0].sentiment_score + '</dd>');
                var entities = $('<dt>' + Drupal.t('Entities') + '</dt><dd><ul></ul></dd>');
                var themes = $('<dt>' + Drupal.t('Themes') + '</dt><dd><ul></ul></dd>');
                var topics = $('<dt>' + Drupal.t('Topics') + '</dt><dd><ul></ul></dd>');
                var categories = $('<dt>' + Drupal.t('Categories') + '</dt><dd><ul></ul></dd>');

                if (typeof(data[0].entities) !== 'undefined') {
                  $.each(data[0].entities, function(idx, value) {
                    var data = [];
                    data.push('<strong>' + Drupal.t('Label') +':</strong> ' + value.label);
                    data.push('<strong>' + Drupal.t('Type') +':</strong> ' + value.type);
                    data.push('<strong>' + Drupal.t('Title') +':</strong> ' + value.title);
                    data.push('<strong>' + Drupal.t('Sentiment') +':</strong> ' + value.sentiment_polarity);
                    data.push('<strong>' + Drupal.t('Sentiment Score') +':</strong> ' + value.sentiment_score);
                    data.push('<strong>' + Drupal.t('Evidence') +':</strong> ' + value.evidence);
                    $('<li>' + data.join('; ') + ' </li>').appendTo(entities.find('ul'));
                  });
                }
                else {
                  $('<li>' + Drupal.t('None found.') + ' </li>').appendTo(entities.find('ul'));
                }
                if (typeof(data[0].themes) != 'undefined') {
                  $.each(data[0].themes, function(idx, value) {
                    var data = [];
                    data.push('<strong>' + Drupal.t('Title') +':</strong> ' + value.title);
                    data.push('<strong>' + Drupal.t('Sentiment') +':</strong> ' + value.sentiment_polarity);
                    data.push('<strong>' + Drupal.t('Sentiment Score') +':</strong> ' + value.sentiment_score);
                    data.push('<strong>' + Drupal.t('Evidence') +':</strong> ' + value.evidence);
                    data.push('<strong>' + Drupal.t('Strength Score') +':</strong> ' + value.strength_score);
                    $('<li>' + data.join('; ') + ' </li>').appendTo(themes.find('ul'));
                  });
                }
                else {
                  $('<li>' + Drupal.t('None found.') + ' </li>').appendTo(themes.find('ul'));
                }
                if (typeof(data[0].topics) != 'undefined') {
                  $.each(data[0].topics, function(idx, value) {
                    var data = [];
                    data.push('<strong>' + Drupal.t('Title') +':</strong> ' + value.title);
                    data.push('<strong>' + Drupal.t('Sentiment') +':</strong> ' + value.sentiment_polarity);
                    data.push('<strong>' + Drupal.t('Score') +':</strong> ' + value.sentiment_score);
                    data.push('<strong>' + Drupal.t('Hit Count') +':</strong> ' + value.hitcount);
                    $('<li>' + data.join('; ') + ' </li>').appendTo(topics.find('ul'));
                  });
                }
                else {
                  $('<li>' + Drupal.t('None found.') + ' </li>').appendTo(topics.find('ul'));
                }
                if (typeof(data[0].auto_categories) != 'undefined') {
                  $.each(data[0].auto_categories, function(idx, value) {
                    var data = [];
                    data.push('<strong>' + Drupal.t('Title') +':</strong> ' + value.title);
                    data.push('<strong>' + Drupal.t('Sentiment') +':</strong> ' + value.sentiment_polarity);
                    data.push('<strong>' + Drupal.t('Score') +':</strong> ' + value.sentiment_score);
                    data.push('<strong>' + Drupal.t('Strength Score') +':</strong> ' + value.strength_score);
                    $('<li>' + data.join('; ') + ' </li>').appendTo(categories.find('ul'));
                  });
                }
                else {
                  $('<li>' + Drupal.t('None found.') + ' </li>').appendTo(categories.find('ul'));
                }

                // Sets the results container with the content.
                var dl = $('<dl></dl>').append(configuration_id).append(status).append(sentiment).append(score).append(entities).append(themes).append(topics).append(categories);
                $('.semantria-testing-form__response', form).empty().append(dl);

              }

            });

          };

          // Runs checks.
          check_for_semantria_response();

        }

      });
    }
  }

})(jQuery);