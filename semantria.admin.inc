<?php

/**
 * Semantria settings configuration form.
 */
function semantria_settings_form($form, &$form_state) {

  $form['api_integration'] = array(
    '#description' => t('You should have received these credentials on an email as soon as you created a Semantria account.'),
    '#title' => t('API Integration'),
    '#type' => 'fieldset',
    'semantria_api_key' => array(
      '#default_value' => variable_get('semantria_api_key', NULL),
      '#title' => t('API Key'),
      '#type' => 'textfield',
    ),
    'semantria_api_secret' => array(
      '#default_value' => variable_get('semantria_api_secret', NULL),
      '#title' => t('API Secret'),
      '#type' => 'textfield',
    ),
  );

  try {
    $configurations = _semantria_get_available_configurations();
    foreach ($configurations as &$conf) {
      $conf = $conf['name'];
    }
    $form['settings'] = array(
      '#title' => 'Settings',
      '#type' => 'fieldset',
      'semantria_default_configuration' => array(
        '#default_value' => variable_get('semantria_default_configuration', NULL),
        '#description' => t('Select the configuration that will be used when sending content to be evaluated.'),
        '#options' => $configurations,
        '#required' => TRUE,
        '#title' => t('Default Configuration'),
        '#type' => 'select',
      ),
      'semantria_max_document_size' => array(
        '#default_value' => variable_get('semantria_max_document_size', 2048),
        '#description' => t('Select the max document size based on your Semantria agreement. Sending a document over the agreement limit will cause the document to not be processed by Semantria.'),
        '#options' => array(2048 => '2KB', 4096 => '4KB', 8192 => '8KB'),
        '#title' => t('Max Document Size'),
        '#type' => 'select',
      ),
      'semantria_debug_status' => array(
        '#default_value' => variable_get('semantria_debug_status', 2048),
        '#description' => t('Enable this in order to store in logs the responses received from Semantria.'),
        '#options' => array(1 => t('Enabled'), 0 => t('Disabled')),
        '#title' => t('Debug mode'),
        '#type' => 'radios',
      ),
    );

  }
  catch (Exception $ex) {
    drupal_set_message($ex->getMessage(), 'error');
  }

  return system_settings_form($form);

}

/**
 * Semantria testing form.
 */
function semantria_testing_form($form, &$form_state) {

  $configurations = _semantria_get_available_configurations();
  $current_configuration = $configurations[variable_get('semantria_default_configuration')];
  foreach ($configurations as &$conf) {
    $conf = $conf['name'];
  }

  // The callback URL that must be set as part of the Semantria configuration. It must be HTTPs.
  $callback_url = str_replace('http://', 'https://', url('semantria/callback.json', array('absolute' => TRUE)));

  // Adds class for JS interaction.
  $form['#attributes']['class'][] = 'semantria-testing-form';

  // Builds form.
  $form['test_fieldset'] = array(
    '#description' => t('You can use this to test content and get the results from the Semantria service.'),
    '#type' => 'fieldset',
    '#title' => t('Semantria integration testing'),
    'configuration' => array(
      '#markup' => t('<strong>Default Configuration:</strong> @name (@id)', array('@id' => $current_configuration['id'], '@name' => $current_configuration['name'])),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ),
    'max_document_size' => array(
      '#markup' => t('<strong>Max Document Size:</strong> @max_document_size', array('@max_document_size' => variable_get('semantria_max_document_size', 2048))),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ),
    'configurations' => array(
      '#description' => t('In order to test, the callback url must be set to @callback_url, otherwise Semantria will not send the responses.', array('@callback_url' => $callback_url)),
      '#default_value' => $current_configuration['id'],
      '#options' => $configurations,
      '#title' => t('Configuration to test'),
      '#type' => 'select',
    ),
    'content' => array(
      '#description' => t('Paste the content you want to evaluate here. Keep in mind that you are bound to the limits of your Lexalytics agreement regarding the content to submit.'),
      '#rows' => 15,
      '#title' => t('Content to Evaluate'),
      '#type' => 'textarea',
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    ),
  );

  // After a submission, stores the identifier.
  if (isset($form_state['values']['semantria_id'])) {
    $form['unique_id'] = array(
      '#type' => 'hidden',
      '#value' => $form_state['values']['semantria_id'],
    );
    $form['response'] = array(
      '#title' => t('Semantria Response'),
      '#type' => 'fieldset',
      '#weight' => -1,
      'data' => array(
        '#markup' => t('Waiting from Semantria to submit data.') . '<div class="ajax-progress"><div class="throbber"></div></div>',
        '#prefix' => '<div class="semantria-testing-form__response">',
        '#suffix' => '</div>',
      ),
    );
  }

  // Adds the JS that handles response checking.
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'semantria') . '/js/semantria.admin.js'
  );

  return $form;

}

/**
 * Validation handler for semantria_testing_form().
 */
function semantria_testing_form_validate($form, &$form_state) {

  // Verifies the document is within the size limit.
  $content = trim($form_state['values']['content']);
  $max_document_size = variable_get('semantria_max_document_size', 2048);
  if (strlen($content) > $max_document_size) {
    drupal_set_message(t('Document size is @size. It is over the specified document size.', array('@size' => strlen($content))), 'warning');
  }
  $form_state['values']['content'] = $content;

}

/**
 * Submission handler for semantria_testing_form().
 */
function semantria_testing_form_submit($form, &$form_state) {

  // Does not rebuild the form.
  $form_state['rebuild'] = TRUE;

  // Updates with a submission id and adds it to the values for later usage.
  $unique_id = uniqid('', TRUE);
  $form_state['values']['semantria_id'] = $unique_id;

  // Enqueues a single document.
  $content = $form_state['values']['content'];
  $result = semantria_send_plain_document($content, array('_semantria_filename' => "{$unique_id}", '_semantria_is_test' => TRUE));
  if ($result != 202) {
    $callback_handler = _semantria_get_session_callback_handler();
    $data = $callback_handler->getErrorData();
    drupal_set_message(t('An error ocurred when submitting the data to the Semantria Service: Code @code - @message.', array('@code' => $data['status'], '@message' => $data['message'])), 'error');
  }
  else {
    drupal_set_message(t('Semantria has received and enqueued the document for processing.'));
  }

}

/**
 * Verifies if a response from Semantria was received and loads it.
 * @param string $unique_id
 *  - The response identifier.
 */
function _semantria_testing_response_check($unique_id) {

  // JSON response.
  drupal_add_http_header('Content-Type', 'application/json');

  // Outputs content if found.
  if (file_exists($file = file_directory_temp() . "/response.{$unique_id}.txt")) {
    $contents = file_get_contents($file);
    echo $contents;
  }

  drupal_exit(0);

}