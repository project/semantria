<?php

/**
 * Class SemantriaSessionCallbackHandler
 *
 * Implementation of a CallbackHandler used to get data regarding the Semantria workflow.
 */
class SemantriaSessionCallbackHandler extends \Semantria\CallbackHandler {

  private $_data;

  function onRequest($sender, $args) {
    //$s = json_encode($args);
    //echo "REQUEST: ", htmlspecialchars($s), "\r\n";
  }

  function onResponse($sender, $args) {
    //$s = json_encode($args);
    //echo "RESPONSE: ", htmlspecialchars($s), "\r\n";
  }

  function onError($sender, $args) {
    $this->_data = $args;
  }

  function onDocsAutoResponse($sender, $args) {
    //$s = json_encode($args);
    //echo "DOCS AUTORESPONSE: ", htmlspecialchars($s), "\r\n";
  }

  function onCollsAutoResponse($sender, $args) {
    //$s = json_encode($args);
    //echo "COLLS AUTORESPONSE: ", htmlspecialchars($s), "\r\n";
  }

  function getErrorData() {
    return $this->_data;
  }

}