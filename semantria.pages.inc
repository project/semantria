<?php

/**
 * Processes a response from Semantria.
 */
function _semantria_callback_process() {

  // Responds with a status code.
  http_response_code(200);

  // Gets the information.
  $parameters = drupal_get_query_parameters();
  $contents = file_get_contents('php://input');

  // Logs response.
  if (variable_get('semantria_debug_status', FALSE)) {
    watchdog('semantria', 'Semantria callback handler: Received data: @data', array('@data' => $contents));
  }

  // Converts contents for processing.
  $contents = json_decode($contents);

  // Stitches documents back into a single response.
  $stitched_documents = array();
  $fields_that_could_be_missing = array('themes', 'entities', 'topics', 'auto_categories');
  foreach ($contents as $processed_document) {
    $id = explode('__', $processed_document->id)[0];
    if (!isset($stitched_documents[$id])) {
      $processed_document->id = $id;
      $stitched_documents[$id] = $processed_document;
      $stitched_documents[$id]->processed_pieces = 1;
      foreach ($fields_that_could_be_missing as $field) {
        $stitched_documents[$id]->$field = array();
      }
    }
    else {
      foreach ($fields_that_could_be_missing as $field) {
        if (isset($processed_document->$field)) {
          $stitched_documents[$id]->$field = array_merge($stitched_documents[$id]->$field, $processed_document->$field);
        }
      }
      $stitched_documents[$id]->processed_pieces++;
    }
  }
  $stitched_documents = array_values($stitched_documents);

  // We check if it was a test from the UI in order to save the document that will be requested later.
  $first = current($stitched_documents);
  if (isset($first->metadata->_semantria_is_test) && $first->metadata->_semantria_is_test) {
    $filename = preg_replace('/[^a-zA-Z0-9\.]/', '', $first->metadata->_semantria_filename);
    file_put_contents(file_directory_temp() . "/response.{$filename}.txt", json_encode($stitched_documents));
  }

  // Shares the information with the rest of the modules trough a hook.
  module_invoke_all('semantria_process_response_documents', $stitched_documents);

}
