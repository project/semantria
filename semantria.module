<?php

include 'semantria.helper.inc';

/**
 * Implements hook_permissions().
 */
function semantria_permissions() {
  return array(
    'administer semantria' => array(
      'title' => t('Administer Semantria'),
      'description' => t('Administers Semantria integration settings.'),
    ),
    'test semantria' => array(
      'title' => t('Test Semantria Integration'),
      'description' => t('Access the testing page.'),

    ),
  );
}

/**
 * Implements hook_menu().
 */
function semantria_menu() {

  $items = array();

  // Configuration page.
  $items['admin/config/services/semantria'] = array(
    'title' => 'Semantria',
    'description' => 'Provides configuration options for the Semantria service integration.',
    'access arguments' => array('administer semantria settings'),
    'file' => 'semantria.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('semantria_settings_form'),
    'type' => MENU_NORMAL_ITEM,
  );

  // Testing page.
  $items['admin/reports/semantria'] = array(
    'title' => 'Semantria Testing',
    'description' => 'A simple test page to check semantria is working properly.',
    'access arguments' => array('test semantria'),
    'file' => 'semantria.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('semantria_testing_form'),
    'type' => MENU_NORMAL_ITEM,
  );

  // Response check from the testing page (JS).
  $items['admin/reports/semantria/response-check/%'] = array(
    'title' => 'Semantria Testing Response',
    'description' => 'A callback that loads the response content from semantria.',
    'access arguments' => array('test semantria'),
    'file' => 'semantria.admin.inc',
    'page arguments' => array(4),
    'page callback' => '_semantria_testing_response_check',
    'type' => MENU_CALLBACK,
  );

  // Callback URL handler.
  $items['semantria/callback.json'] = array(
    'title' => 'Semantria Callback Handler',
    'description' => 'Processes the response from the Semantria service.',
    'access callback' => TRUE,
    'file' => 'semantria.pages.inc',
    'page callback' => '_semantria_callback_process',
    'type' => MENU_CALLBACK,
  );

  return $items;

}

/**
 * Sends a single document to the Semantria Service.
 *
 * @param string $content
 *  - The text content to send.
 * @param mixed $metadata
 *  - Any particular information that you want to attach to the document.
 * @param null $identifier
 */
function semantria_send_plain_document($content, $metadata = array()) {

  // Performs cleanup.
  $content = trim(strip_tags($content));

  // Builds document to send.
  $tasks = _semantria_build_tasks_from_content($content, $metadata);

  // Sends the tasks.
  return _semantria_send_tasks($tasks);

}

/**
 * Sends multiple documents to the Semantria Service.
 *
 * @param string $content
 *  - The text content to send.
 * @param null $identifier
 */
function semantria_send_plain_batch($documents) {

  if (empty($documents)) {
    throw new Exception('No documents have been provided.');
  }

  // Builds document to send.
  $tasks = array();
  foreach ($documents as $doc) {
    $text = $doc['text'];
    $metadata = isset($doc['metadata']) ? $doc['metadata'] : array();
    $tasks = array_merge($tasks, _semantria_build_tasks_from_content($text, $metadata));
  }

  return _semantria_send_tasks($tasks);

}
