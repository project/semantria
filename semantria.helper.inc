<?php

/**
 * @return SemantriaSessionCallbackHandler
 *  A Semantria Session Callback Handler for the active session.
 */
function _semantria_get_session_callback_handler() {
  $semantria_callback_handler = &drupal_static('semantria_session_callback_handler', NULL);
  if (!$semantria_callback_handler) {
    $semantria_callback_handler = new SemantriaSessionCallbackHandler();
  }
  return $semantria_callback_handler;
}

/**
 * Gets an initialized semantria session.
 */
function _semantria_get_session() {
  $semantria_session = &drupal_static('semantria_session', NULL);
  if (!$semantria_session) {
    $semantria_session = new \Semantria\Session(variable_get('semantria_api_key', NULL), variable_get('semantria_api_secret', NULL), NULL, NULL, TRUE);
    $callback_handler = _semantria_get_session_callback_handler();
    $semantria_session->setCallbackHandler($callback_handler);
  }
  return $semantria_session;
}

/**
 * Gets all available configurations from Semantria.
 */
function _semantria_get_available_configurations() {

  $configurations = &drupal_static('semantria_configurations', array());

  if (empty($configurations)) {
    $semantria_session = _semantria_get_session();
    foreach ($semantria_session->getConfigurations() as $configuration) {
      $configurations[$configuration['id']] = $configuration;
    }
    uasort($configurations, function($a, $b) {
      return strcmp($a['name'], $b['name']);
    });
  }
  return $configurations;

}

/**
 * Converts content into a tasks.
 *
 * @param string $content
 *   The content to submit that will be converted into tasks.
 * @param string $identifier
 *   The base identifier of the tasks.
 * @return mixed
 *   An array of associative arrays with the following keys:
 *   - "id": Required. The document's id.
 *   - "text": Required. The document's text.
 * @throws Exception
 */
function _semantria_build_tasks_from_content($content, $metadata = array()) {

  if (empty(trim($content))) {
    throw new Exception('Content to submit cannot be empty.');
  }

  // Sets a unique identifier.
  $identifier = uniqid("", TRUE);

  // Builds the tasks.
  $content = _semantria_split_content_in_chunks($content, variable_get('semantria_max_document_size', 2048));
  foreach ($content as $idx => &$task) {
    $task = array('id' => "{$identifier}__{$idx}", 'text' => $task);
    if (!empty($metadata)) {
      $task['metadata'] = $metadata;
    }
  }

  return $content;

}

/**
 * @param $content
 * @param $max_document_length
 * @return string[]
 *   A list of strings with length within the limit and making sure that words are not broken.
 */
function _semantria_split_content_in_chunks($content, $max_document_length) {

  // Some basic cleanup.
  $content = trim(str_replace(array(PHP_EOL, '  '), ' ', $content));

  // Builds chunks.
  $content_chunks = array();
  if (strlen($content) <= $max_document_length) {
    $content_chunks[] = $content;
  }
  else {
    $content = explode(' ', $content);
    $chunk = array();
    $total_length = 0;
    foreach ($content as $word) {
      $word_length = strlen($word);
      $with_word_is_exactly_in_limit = $total_length + $word_length === $max_document_length;
      $with_word_is_over_limit = $total_length + $word_length > $max_document_length;
      if ($with_word_is_exactly_in_limit || $with_word_is_over_limit) {
        if ($with_word_is_exactly_in_limit) {
          $chunk[] = $word;
        }
        $content_chunks[] = join(' ', $chunk);
        $total_length = 0;
        $chunk = array();
      }
      else {
        $chunk[] = $word;
        $total_length += $word_length + 1;
      }
    }
    if (!empty($chunk)) {
      $content_chunks[] = join(' ', $chunk);
    }
  }

  return $content_chunks;

}

/**
 * Sends a tasks to Semantria for processing.
 */
function _semantria_send_tasks($tasks) {

  // Sends documents.
  $semantria_session = _semantria_get_session();
  $configuration = variable_get('semantria_default_configuration', NULL);
  $result = NULL;
  if (count($tasks) > 1) {
    $result = $semantria_session->queueBatch($tasks, $configuration);
  }
  else {
    $result = $semantria_session->queueDocument($tasks[0], $configuration);
  }

  return $result;

}
